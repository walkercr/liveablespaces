package com.herokuapp.liveablespaces;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * Application entry point.
 *
 * @author Craig Walker
 * @version 1.0
 * @since 9/3/2017
 */
@SpringBootApplication
@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
public class Application {

    /**
     * Main method.
     * @param args program arguments
     */
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
