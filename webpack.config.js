var webpack = require('webpack');
var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');

const PATH = {
    BUILD_DIR: path.resolve(__dirname, 'src/main/resources/static'),
    SRC_DIR: path.resolve(__dirname, 'src/main/react'),
    ENTRY: path.resolve(__dirname, 'src/main/react/index.jsx')
};

// this create a index.html in resources.build directory based on specified template
var HTMLWebpackPluginConfig = new HtmlWebpackPlugin({
    template: __dirname + '/index.html',
    filename: 'index.html',
    inject: 'body'
});

const CONFIG = {
    entry: PATH.ENTRY,
    output: {
        path: PATH.BUILD_DIR,
        filename: 'bundle.js'
    },
    module: {
        preLoaders: [
            {
                test: /(\.jsx$|\.js)$/,
                loader: 'eslint-loader',
                include: PATH.SRC_DIR
            }
        ],
        loaders: [
            {
                test: /(\.js|\.jsx)$/,
                loader: 'babel',
                include: PATH.SRC_DIR
            },
            {
                test: /\.s?css$/,
                loaders: ['style', 'css', 'sass']
            }
        ]
    },
    plugins: [
        HTMLWebpackPluginConfig,
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify('production')
            }
        }),
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            }
        })
    ]
};

module.exports = CONFIG;