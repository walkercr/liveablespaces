# Getting Started
This page documents the steps necessary to get the application up and running.

## Git Repository
* GitHub: [https://github.com/walkercr/LiveableSpaces.git](https://github.com/walkercr/LiveableSpaces.git)

## Dependencies
* Java 1.8
* Database 
* All other dependencies are locally contained in non-IBM-server environment (i.e. Gradle, Node, NPM)

## Caution
* The commands listed on this page follow Unix syntax.
* Use `gradlew` instead of `./gradlew` on a Windows machine.
        
## Build Commands
* `./gradlew build` - Builds the Java back-end.
* `./gradlew npmBuild` - Builds the React front-end.
* `./gradlew buildAll` - Builds the entire project (front-end and back-end).
    - equivalent to `./gradlew npmBuild & ./gradlew build`
* `./gradlew jar` - Builds and packs the entire project into a fat jar.

## Clean Commands
* `./gradlew clean` - Deletes all build artifacts in the 'build/' directory
* `./gradlew npmCleanBuild` - Deletes React front-end build artifacts in 'resources/static/' directory.
* `./gradlew npmCleanNodeModules` - Deletes front-end 'node_modules/ directory.
* `./gradlew npmCleanAll` - Deletes all front-end build artifacts and resources.
    - equivalent to `./gradlew npmCleanBuild & ./gradlew npmCleanNodeModules`
* `./gradlew cleanAll` - Deletes all project build artifacts and resources.
    - equivalent to `./gradlew clean & ./gradlew npmCleanAll`

## Run Commands
* `./gradlew bootRun` - Run as an unjarred spring-boot application.
* `java -jar build/libs/Capstone_IBM01.jar` - Run as a fat jar spring-boot application.

## Front-End Development Commands
* `./gradlew npmInstall` - Installs a locally contained version of Node and NPM.
    - This task is included in both npmBuild and buildAll.
    - It is only necessary if local versions of Node and NPM have not yet been downloaded.
* `./gradlew npmStart` - Starts a hot reloading front-end dev server on 'localhost:9000'.
    - Hit `Ctrl + C` to stop the dev server.

## Who do I talk to?
* Craig Walker - [craig.allen.walker@gmail.com](mailto:craig.allen.walker@gmail.com)